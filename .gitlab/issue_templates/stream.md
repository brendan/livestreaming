# Live Stream

## Pre-launch checklist

### General
* [ ] Get water
* [ ] Start OBS
* [ ] Set DnD on laptop
* [ ] Set DnD on `Computer` (Alexa)
* [ ] Turn off mic on `Computer` (Alexa)
* [ ] Spotify to `Gaming Music for YouTube...` playlist
* [ ] Open [this issue template](https://gitlab.com/brendan/livestreaming/-/edit/master/.gitlab/issue_templates/stream.md) for updating
* [ ] Connect AirPods
* [ ] Launch `WaveLink`
* [ ] Get iPad

### Displays

#### Laptop monitor
* [ ] OBS on the right
* [ ] [Open Twitch stream manager](https://dashboard.twitch.tv/u/olearycrew/stream-manager)
* [ ] Pop out chat

#### Large monitor
* [ ] Whatever you're coding in
* [ ] Move slack / close slack
* [ ] Move everything you're not using to the laptop monitor

#### 

## Go Live Procedure
* [ ] Move this list to laptop monitor
* [ ] Stream deck to Streaming screen
* [ ] Tweet / Tweet button
* [ ] Share in `#content-share` in DevRel Collective
* [ ] Put OBS on "Starting Soon" scene
* [ ] Mic / Stream output check
* [ ] Mute Mic
* [ ] Click "Stream"
* [ ] Start 5 minute timer
* [ ] Get stream dashboard up on the iPad
* [ ] Fade music

### Specific Topics
